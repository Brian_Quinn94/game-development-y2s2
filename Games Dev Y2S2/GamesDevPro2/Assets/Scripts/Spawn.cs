﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {

    PlayerMovement CurrentHealth;
    public GameObject Enemy1;
    public GameObject Enemy2;
    public float spawnTime = 3f;
    public SpawnPoint[] spawnPoints;
    public Transform[] enemyType;
    private PlayerMovement player;


    //Enemy enemyType = new enemyType;  
    // Use this for initialization
    void Start()
    {
        spawnPoints = FindObjectsOfType<SpawnPoint>();
        print(spawnPoints.Length);
        player = FindObjectOfType<PlayerMovement>();
        
        InvokeRepeating("SpawnEnemy", spawnTime, spawnTime);
    }


    void SpawnEnemy()
    {


        int spawnPointIndex = Random.Range(0, spawnPoints.Length);
        
           
        //int enemyType = Random.Range(0, EnemyTypes.Length);
        Instantiate(Enemy1, spawnPoints[spawnPointIndex].transform.position, spawnPoints[spawnPointIndex].transform.rotation);
        Instantiate(Enemy2, spawnPoints[spawnPointIndex].transform.position, spawnPoints[spawnPointIndex].transform.rotation);
    }
}
