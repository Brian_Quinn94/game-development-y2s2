﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCAI2 : MonoBehaviour {


    PlayerMovement CurrentHealth;
    public float MoveSpeed = 10;
    public int maxhealth = 100;
    public int currentHealth;
    private int score = 10;
    private PlayerMovement player;

    Vector3 direction;
    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
        currentHealth = maxhealth;
    }

    // Update is called once per frame
    void Update()
    {

        direction = player.transform.position - transform.position;
        direction.Normalize();
        transform.rotation = Quaternion.LookRotation(direction);
        transform.position += direction * MoveSpeed * Time.deltaTime;

    }
    public void OnCollisionEnter(Collision col)
    {

        bullet bulletInfo = col.collider.gameObject.GetComponent<bullet>();

        if (bulletInfo)
        {

            //Debug.Log("hit taken");
            currentHealth = currentHealth - 20;
            bulletInfo.hit();
        }

        if (currentHealth <= 0) {

            Destroy(this.gameObject);
            //score ++;

        }
        else
        {
            //Debug.Log("Missed");
        }
        NPCbullet NPCbulletInfo = col.collider.gameObject.GetComponent<NPCbullet>();

        if (NPCbulletInfo)
        {

            NPCbulletInfo.hit();
        }

    }
}
