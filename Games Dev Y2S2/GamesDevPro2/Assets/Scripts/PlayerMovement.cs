﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{

    //declaring base varibles for character control
    public float MoveSpeed = 20.0f;
    //public float gravity = 20.0f;
    public int MaxHealth = 100;
    private int CurrentHealth;
    public GUIElement Slider;
    private int scoreCount = 0;
    private int WaveCount = 1;

    //Player movement keys
    private KeyCode upKey = KeyCode.W;
    private KeyCode downKey = KeyCode.S;
    private KeyCode LeftKey = KeyCode.A;
    private KeyCode RightKey = KeyCode.D;

    //Player States
    private bool canControl = true;
    private bool isDead = false;

    private Vector3 MoveDir = new Vector3();
    private Vector3 velocity = new Vector3();
    public CharacterController controller;

    public GUIText Score;
    private int score;


    void Start()
    {
        CurrentHealth = MaxHealth;
        controller = GetComponent<CharacterController>();
        score = 0;
        UpdateScore();
    }

    // Update is called once per frame
    void Update()
    {
        Ray look = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        Physics.Raycast(look, out hitInfo);
        transform.rotation = Quaternion.LookRotation(new Vector3(hitInfo.point.x, transform.position.y, hitInfo.point.z) - transform.position);
        Vector3 right = Camera.main.transform.right;
        right.y = 0;
        right.Normalize();
        Vector3 forward = new Vector3(Camera.main.transform.forward.x, 0, Camera.main.transform.forward.z).normalized;

        Vector3 velocity = right * (Input.GetAxis("Horizontal")) + forward * (Input.GetAxis("Vertical"));

        velocity = MoveSpeed * velocity.normalized;

        transform.position += velocity * Time.deltaTime;

        }
        public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {
        Score.text = "Score: " + score;
    }

    public void OnCollisionEnter(Collision col)
    {

        NPCbullet bulletInfo = col.collider.gameObject.GetComponent<NPCbullet>();

        if (bulletInfo)
        {

            //Debug.Log("hit taken");
            CurrentHealth = CurrentHealth - 20;
            bulletInfo.hit();
        }

        else if (CurrentHealth <= 0)
        {

            Destroy(this.gameObject);
            

        }


    }
}
