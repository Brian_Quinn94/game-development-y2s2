﻿
using UnityEngine;
using System.Collections;

public class NPCAI : MonoBehaviour {

    public GameObject player;

    public float MoveSpeed = 10;
    public int health = 100;

    Vector3 direction;
    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        direction = player.transform.position - transform.position;
        direction.Normalize();
        transform.rotation = Quaternion.LookRotation(direction);
        transform.position += direction * MoveSpeed * Time.deltaTime;

    }
    public void OnCollisionEnter(Collision col)
    {



            bullet bulletInfo = col.collider.gameObject.GetComponent<bullet>();

            if(bulletInfo)
        { 

            Debug.Log("hit taken");
            Destroy(this.gameObject);
            bulletInfo.hit();
        }
        else
        {
           // Debug.Log("Missed");
        }
       // print("ouch");
    }

}
