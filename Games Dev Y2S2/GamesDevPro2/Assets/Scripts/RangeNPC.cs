﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeNPC : MonoBehaviour {


    private float range = 200f;
    private float shotSpeed = 200f;
    private float time = 1f;
    private int maxhealth = 20;
    private int currentHealth;
    private float timer = 0f;
    private PlayerMovement player;
    public GameObject NPCbullet;


    Vector3 direction;

    // Use this for initialization
    void Start () {
        player = FindObjectOfType<PlayerMovement>();
        currentHealth = maxhealth;

    }
	
	// Update is called once per frame
	void Update () {
        direction = player.transform.position - transform.position;
        direction.Normalize();
        transform.rotation = Quaternion.LookRotation(direction);

        if (Vector3.Distance(transform.position, player.transform.position) < range)
        {

            Aim();
            timer += Time.deltaTime;
            print(timer + " , "+ time);
            if (timer >= time)
            {
                Shoot();
                timer = 0;
            }
        }
        else { timer = 0f; }


    }


    private void Aim() {
        Vector3 dir = player.transform.position - transform.position;
        Quaternion rot = Quaternion.LookRotation(dir);
        Vector3 eulerRot = rot.eulerAngles;
        
    }

    void OnTriggerEnter(Collider Player)
    {
        Destroy(NPCbullet.gameObject);
    }
    private void Shoot()
    {
        if (Vector3.Distance(transform.position, player.transform.position) <= range)
        {
            GameObject instantiatedProjectile = (GameObject)Instantiate(NPCbullet, transform.position + 2 * transform.forward, transform.rotation);
            instantiatedProjectile.GetComponent<Rigidbody>().velocity = transform.TransformDirection(new Vector3(0, 0, shotSpeed));
            //Vector3 shootDirection = Input.mousePosition - transform.position;
        }
    }

    public void OnCollisionEnter(Collision col)
    {

        bullet bulletInfo = col.collider.gameObject.GetComponent<bullet>();

        if (bulletInfo)
        {

            //Debug.Log("hit taken");
            currentHealth = currentHealth - 20;
            bulletInfo.hit();
        }

        if (currentHealth <= 0)
        {

            Destroy(this.gameObject);
            //score ++;

        }
        else
        {
            //Debug.Log("Missed");
        }


    }
   }
