﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundry : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnCollisionEnter(Collision col)
    {
        bullet bulletInfo = col.collider.gameObject.GetComponent<bullet>();

        if (bulletInfo)
        {
 
            bulletInfo.hit();
        }
        NPCbullet NPCbulletInfo = col.collider.gameObject.GetComponent<NPCbullet>();

        if (NPCbulletInfo)
        {

            NPCbulletInfo.hit();
        }

    }

}
